# Test frontend BitBoss

Benvenuto al test di frontend di BitBoss. Leggi fino alla fine per ottenere tutte le informazioni necessarie su come completare il test.

## Target
Il target di questo progetto è quello di creare un markup come quello di questo screenshot (trovi l'immagine a piena risoluzione nella route del progetto) partendo dal componente vuoto che ti è stato preparato.

Cliccando su un elemento bisogna eseguire un'azione di qualunque genere. Azioni valide sono:
- console.log dell'utente
- apertura della finestra di invio email con l'indirizzo dell'utente fornito
- apertura in una nuova scheda dell'immagine del profilo

Se per altri motivi vuoi aggiungere, togliere, modificare componenti hai piena libertà di farlo.

Ricordati di fare un appunto delle tue scelte.
![Immagine del componente da ricreare](./reference.png "Screenshot")


## Strumenti
Gli unici strumenti obbligatori sono Tailwind e Vue 3.
Tutto il resto è lasciato alla tua discrezione. Vuoi utilizzare TypeScript? Perfetto. Vuoi utilizzare Javascript? Va bene anche così.

Trovi il JSON da cui partire all'interno del file `App.vue`.

## Conclusione
Quando pensi di aver concluso il test **apri una Pull Request** sul repository a attendi una nostra risposta.

Grazie e in bocca al lupo.
